<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body>
<jsp:useBean id="test" class="beans.Testbeans" scope="page"></jsp:useBean>
<p><c:out value="${test.info}"/></p>

</p>
<c:out value="${param.name}"/>

</p>
<c:if test='${param.name=="Josheela"}'>
Hello Josheela!
</c:if>
</p>
<c:choose>
<c:when test='${param.id=="1"}'>
<b>Param is 1</b>
</c:when>
<c:when test='${param.id=="2"}'>
<b>Param is 2</b>
</c:when>
<c:otherwise>Param is neither 1 nor 2</c:otherwise>
</c:choose>

<c:forEach var="i" begin="1" end ="10" step="2" varStatus="status">

</br>Counter is :<c:out value="${i}"/>
<c:if test="${status.first}">This is the first iteration</c:if>
<c:if test="${status.last}">This is the last iteration</c:if>
</c:forEach>



</body>
</html>